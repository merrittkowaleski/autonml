Auto\ :sup:`n`\  ML
======================

Welcome to the documentation site for CMU TA2 Auto\ :sup:`n`\  ML. Built using DARPA D3M ecosystem

Overview
--------
Auto\ :sup:`n`\ ML is an automated machine learning system developed by CMU Auton Lab 
to power data scientists with efficient model discovery and advanced data analytics.  

Taking your machine learning capacity to the nth power.


.. image:: docs/img/model_pipeline.png
   :scale: 50%
   :alt: CMU TA2 Pipeline
