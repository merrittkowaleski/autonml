.. image:: img/AutonML_logo.png
   :scale: 50%
   :alt: CMU TA2 Pipeline

Welcome to the documentation site for CMU TA2 Auto\ :sup:`n`\  ML. 

Built using DARPA D3M ecosystem

Overview
--------
Auto\ :sup:`n`\ ML is an automated machine learning system developed by CMU Auton Lab
to power data scientists with efficient model discovery and advanced data analytics
Auton ML also powers the D3M Subject Matter Expert (SME) User Interfaces such as `Two Ravens <http://2ra.vn/>`_

Taking your machine learning capacity to the nth power

This is how the model works

.. image:: img/model_pipeline.png
   :scale: 50%
   :alt: CMU TA2 Pipeline

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Getting Started
   :name: sec-getting_started

   getting_started
   convert_d3m_data

.. toctree::
   :hidden:
   :maxdepth: 2
   :caption: Auton ML Organization
   :name: sec-organization

   functionalities

